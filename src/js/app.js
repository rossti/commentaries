'use strict';

var app = angular.module('Tabletki', []);

app.controller('CommentsCtrl', ['$scope', function($scope){
    $scope.comments = [
        {
            name: 'Mike Taylor', date: new Date(2016, 3, 3, 13, 22), like: 12,
            img: 'img/profile-photo-mike.jpg',
            message: 'Such a massive change in such a short time is extremely unusual.'
        },
        {
            name: 'Sophia Anderson', date: new Date(2016, 3, 3, 17, 4), like: 8,
            img: 'img/profile-photo-sophia.jpg',
            message: 'Loss of Arctic sea ice is just one of the many changes that are accelerating it.'
        },
        {
            name: 'Charlie Harris', date: new Date(2016, 3, 3, 10, 34), like: 5,
            img: 'img/profile-photo-charlie.jpg',
            message: 'Perhaps they had the same thing in the early 20th century'
        }
    ];

    $scope.addComments = function(){
        $scope.comments.push(
            {
                name: 'User', date: new Date(), like: 0,
                img: 'img/user-big.png',
                message: $scope.message
            }
        );
        $scope.message = '';
    }

}]);
